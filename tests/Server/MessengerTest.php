<?php

namespace Tests\Server;

use App\Server\Entity\Environment\SystemType;
use App\Tests\TestCase;
use Ratchet\ConnectionInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\Server\Application;
use App\Server\ServerState;

class MessengerTest extends TestCase
{
    private $stateMock;
    
    private $outputMock;
    
    /**
     * @var Messenger
     */
    private $messenger;

    protected function setUp()
    {
        parent::setUp();
        $this->outputMock = $this->createMock(ConsoleOutput::class);
        $this->stateMock = $this->createMock(ServerState::class);
        
        ob_start();
        $this->messenger = new Application($this->outputMock, $this->stateMock, Application::ENV_PROD);
        ob_end_clean();
    }

    public function testOnOpen()
    {
        ob_start();
        $connectionMock = $this->createMock(ConnectionInterface::class);
        $connectionMock->resourceId = 1;
        
        $connectionMock->expects($this->exactly(2))
                ->method('send')
                ->with($this->callback(function ($subject) {
                    return (
                            strpos($subject, SystemType::COUNT_CONNECTED_USER) || 
                            strpos($subject, SystemType::COUNT_ACTIVE_ROOM) ||
                            strpos($subject, SystemType::SERVER_START_DATE) 
                        );
                }));
        
        $this->messenger->onOpen($connectionMock);
        ob_end_clean();
    }
}
