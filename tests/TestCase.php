<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

abstract class TestCase extends PHPUnitTestCase
{
    /**
     * Create a mock object of a given class name for concrete classes (non interfaces) which disables the original
     * constructor.
     *
     * @param string $class Class name
     * @param array $methodList A list of methods to mock
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getConcreteMock(string $class, array $methodList = []): \PHPUnit_Framework_MockObject_MockObject
    {
        return $this
            ->getMockBuilder($class)
            ->setMethods($methodList)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Create a mock object of a given abstract class name.
     *
     * @param string $class Class name
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getAbstractMock(string $class): \PHPUnit_Framework_MockObject_MockObject
    {
        $methodList = $this->getMethodList($class);

        return $this
            ->getMockBuilder($class)
            ->setMethods($methodList)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
    }

    /**
     * Make private and protected function callable
     *
     * @param mixed $object   Subject under test
     * @param string $function Function name
     * @return \ReflectionMethod
     */
    protected function makeCallable($object, string $function): \ReflectionMethod
    {
        $method = new \ReflectionMethod($object, $function);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * Sets the given property to given value on Object in Test
     *
     * @param mixed $object Subject under test
     * @param string $name   Property name
     * @return \ReflectionProperty
     */
    protected function makeCallableProperty($object, string $name): \ReflectionProperty
    {
        $property = new \ReflectionProperty($object, $name);
        $property->setAccessible(true);
        return $property;
    }
    
    /**
     * Sets the given property to given value on Object in Test
     *
     * @param mixed $object Subject under test
     * @param string $name   Property name
     * @param mixed $value  Value
     */
    protected function setPropertyOnObject($object, string $name, $value)
    {
        $property = $this->makeCallableProperty($object, $name);
        $property->setValue($object, $value);
    }

    /**
     * Retrieve all the methods from a given class.
     *
     * @param string $class Class Name
     * @return array
     */
    private function getMethodList(string $class): array
    {
        $reducer = function (\ReflectionMethod $method) {
            return $method->getName();
        };

        $reflector = new \ReflectionClass($class);

        return array_map($reducer, $reflector->getMethods());
    }
}
