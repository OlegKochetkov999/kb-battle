import $ from 'jquery';
import 'less';
import KeyboardCore from './kb/core.js';
import C from './resources/constants.js';
const routes = require('../dist/js/fos_js_routing.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);

(function(){
    if ($('#server-status .false').length === 1) {
        return;
    }
    
    let $connectButton = $('#connect');
    let $settingsButton = $('#settings');
    let $stateOptionButton = $('.state-option');
    let $disconnectButton = $('#disconnect');
    let $connectionButtons = $('.connection-button:not(#settings)');
    
    let $countCharsOption = $('#countChars');
    let $countUsersOption = $('#countUsers');
    let $languageOption = $('.language');
    let $speedOption = $('.speed');
    let $loginOption = $('#login');
    
    let connection = {ip: $('#ws_server').val(), port: 8089};
    let kb = new KeyboardCore(connection);
    kb.lg = $languageOption.val();

    function toggleOption(option, isShow) {
        let wrapper = option.parents('.option');
        if (isShow) {
            wrapper.removeClass('hide');
        } else {
            if (!wrapper.hasClass('hide')) {
                wrapper.addClass('hide');
            }
        }
    }
    
    $settingsButton.on('click', function () {
        if (!$(this).hasClass('disabled')) {
            $(this).toggleClass('active');
            $('.menu-block').toggleClass('hide');
        }
    });
    
    $stateOptionButton.on('click', function () {
        if (!$(this).hasClass('disabled')) {
            let block = $(this).data('block');
            $(`.state-option:not([data-block=${block}])`).removeClass('active');
            $(`.header-popup-menu:not(.${block})`).addClass('hide');
            $(this).toggleClass('active');
            $(`.${block}`).toggleClass('hide');
        }
    });
    
    $connectionButtons.on('click', function () {
        if (!$(this).hasClass('disabled')) {
            kb.viewManager.toggleActiveConnectButton();
            if (!$('.menu-block').hasClass('hide')) {
                $settingsButton.toggleClass('active');
                $('.menu-block').toggleClass('hide');
            }
        }
    });
    
    $countUsersOption.on('change', function (e) {
        if ($(e.target).val() == 1) {
            $('#bot-trainig-label').removeClass('hide');
            toggleOption($speedOption, false);
            toggleOption($countCharsOption, true);
        } else {
            $('#bot-trainig-label').addClass('hide');
            toggleOption($speedOption, true);
            toggleOption($countCharsOption, false);
        }
    });
    
    $languageOption.on('change', function (e) {
        let lg = $(e.target).val();
        $.ajax({
            url: Routing.generate('app_change-language'),
            data: {'lg': lg},
            success: function (data) {
                $('.block-keyboard').remove();
                $('.main-block').append(data);
                kb.lg = lg;
            }
        });
    });
    
    $connectButton.on('click', function (e) {
        let countUsers = $countUsersOption.val();
        let type = countUsers == 1 ? C.Parameter.ROOM_TYPE_TRAINING : C.Parameter.ROOM_TYPE_SIMPLE;
        let speed = !$speedOption.hasClass('hide') ? $speedOption.val() : null;
        let chars = !$countCharsOption.hasClass('hide') ? $countCharsOption.val() : null;
        $(e.target).blur();
        
        setTimeout(function() {
            kb.connectUser($loginOption.val());
        }, 700);
        
        setTimeout(function () {
            let options = {};
            options[C.Parameter.COUNT_USERS] = countUsers;
            options[C.Parameter.ROOM_TYPE] = type;
            if (speed) {
                options[C.Parameter.ROOM_SPEED] = speed;
            }
            
            if (chars) {
                options[C.Parameter.COUNT_CHARS] = chars;
            }
            
            kb.connectRoom(options);
        }, 700);
    });
    
    $(document).on('click', '.room-number', function() {
        let options = {};
        options[C.Parameter.ID] = $(this).find('span').data('room');
        options[C.Parameter.COUNT_USERS] = $countUsersOption.val();
        options[C.Parameter.ROOM_TYPE] = C.Parameter.ROOM_TYPE_SIMPLE;
        options[C.Parameter.ROOM_SPEED] = $speedOption.val();
        setTimeout(function() {
            kb.connectUser($loginOption.val());
        }, 700);
        setTimeout(function() {
            kb.connectRoom(options);
            
        }, 700);
        kb.viewManager.toggleActiveConnectButton();
        if (!$('.menu-block').hasClass('hide')) {
            $settingsButton.toggleClass('active');
            $('.menu-block').toggleClass('hide');
        }
        
        $('#count-active-room').trigger('click');
    });
    
    $disconnectButton.on('click', function () {
        kb.disconnectRoom();
    });
})();
