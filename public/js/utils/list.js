/**
 * This class is the List structure
 */
export default class List{
    /**
     * @type integer
     */
    get count() {return this._count;}
    
    /**
     * @type integer
     */
    get capacity() {return this._capacity;}
    
    /**
     * @param {integer} capacity
     */
    constructor(capacity) {
        this._capacity = (capacity || capacity > 0) ? capacity : 10;
        this._count = 0;
        this._array = new Array(this._capacity);
    }
    
    /**
     * Get element of list by index
     *
     * @param {integer} index
     * @returns {mixed}
     */
    get(index) {
        return this._array[index];
    }
    
    /**
     * Add element in end of list
     *
     * @param {mixed} element
     */
    add(element){
        if (this._count >= this._capacity){
            this._capacity *= 2;

            let newArray = new Array(this._capacity);
            for(var i = 0; i < this._count; i++){
                newArray[i] = this._array[i];
            }

            this._array = newArray;
        }

        this._array[this._count] = element;
        this._count++;
    }

    /**
     * Remove element of list by index
     *
     * @param {integer} index
     * @returns {Boolean}
     */
    remove(index){
        if (index >= this._count) {
            return false;
        }

        for(var i = index; i < this._count - 1; i++){
            this._array[i] = this._array[i+1];
        }

        this._array[this._count - 1] = null;
        this._count--;

        return true;
    }
    
    /**
     * Return a new list from current, filtered with callback function
     *
     * @param {function} callback
     * @returns {List}
     */
    find(callback) {
        if (typeof callback != 'function') {
            throw new Error('Argument callback is not a function');
        }
        
        let list = new List(this.capacity/2);

        for (let i = 0; i < this.count; i++) {
            if (callback(this._array[i])) {
                list.add(this._array[i]);
            }
        }

        return list;
    }
    
    /**
     * Check that one or more elements satisfies the conditions in callback function
     *
     * @param {function} callback
     * @returns {Boolean}
     */
    any(callback) {
        if (typeof callback != 'function') {
            throw new Error('Argument callback is not a function');
        }

        for (let i = 0; i < this.count; i++) {
            if (callback(this._array[i])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get index of first element that satisfies the conditions in callback function
     *
     * @param {callback} callback
     * @returns {integer}
     */
    indexOf(callback) {
        if (typeof callback != 'function') {
            throw new Error('Argument callback is not a function');
        }

        for (let i = 0; i < this.count; i++) {
            if (callback(this._array[i])) {
                return i;
            }
        }

        return -1;
    }
    
    /**
     * Check that all elements satisfies the conditions in callback function
     *
     * @param {function} callback
     * @returns {Boolean}
     */
    all(callback) {
        if (typeof callback != 'function') {
            throw new Error('Argument callback is not a function');
        }
        
        for (let i = 0; i < this.count; i++) {
            if (!callback(this._array[i])) {
                return false;
            }
        }

        return true;
    }
}
