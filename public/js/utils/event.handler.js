import List from './list.js';

export default class EventHandler {
    /**
     * @type integer
     */
    get count() {return this._callbackMethods.count;}
    
    constructor(){
        this._callbackMethods = new List(5);
    }
    
    /**
     * Remove callback method by index
     *
     * @param {integer} index
     * @returns {Boolean}
     */
    remove(index) {
        return this._callbackMethods.remove(index);
    }
    
    /**
     * Add callback method
     *
     * @param {function} callback
     * @returns {integer}
     */
    add(callback) {
        this._callbackMethods.add(callback);

        return this._callbackMethods.count;
    }

    /**
     * Executing all callbac methods
     * @param {Object} data
     * @returns {undefined}
     */
    execute(data) {
        for(let i = 0; i < this._callbackMethods.count; i++){
            let callback = this._callbackMethods.get(i);
            callback(data);
        }
    }
}
