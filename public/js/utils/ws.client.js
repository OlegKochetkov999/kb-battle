export default class WSClient {
    constructor(){
        this._webSocket = {};
        this._isConnection = false;
    };

    connect(opt){
        this._webSocket = new WebSocket(`ws://${opt.ip}:${opt.port}/`);

        this._webSocket.onopen = (e) => {
            this._isConnection = true;
            opt.connectCallback();
        };

        this._webSocket.onmessage = function (e) {
            console.log('Receive data: ' + e.data);
            opt.messageCallback(e.data);
        };

        this._webSocket.onerror = function(e) {
            console.log(e);
        };

        this._webSocket.onclose = () => {
            this._isConnection = false;
        };
        
        window.onunload = () => {
            this.disconnect();
        };
        
        window.onbeforeunload = () => {
            this.disconnect();
        };
    };

    disconnect() {
        if (this._isConnection) {
            console.log('event disconnect');
            this._webSocket.close();
        }
    };

    send(data) {
        if (this._isConnection){
            console.log("send data: " + data);
            this._webSocket.send(data);
        }
    };
};