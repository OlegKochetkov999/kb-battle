import wordsEn from '../resources/words.en.js';
import wordsRu from '../resources/words.ru.js';

export default class StringGenerator {
    static generate(countWords, language){
        var count = countWords || 100,
            index,
            result = '',
            words = (language && language === 'en') ? wordsEn : wordsRu;
        
        for (var i = 0; i < count; i++) {
            index = Math.floor(Math.random() * (words.length));
            result += words[index] + " ";
        }

        return result;
    };
}
