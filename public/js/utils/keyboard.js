import EventHandler from './event.handler.js';

export default class Keyboard {
    /**
     * @param {Object} element
     */
    constructor() {
	this._onClickEvent = new EventHandler();
    };
    
    /**
     * @param {Boolean} state
     */
    set enable(state) {
	if (state) {
            if (document.onkeypress == null) {
                document.onkeypress = (e) => {
                    var eventArgs = {};
                    eventArgs.which = e.which;
                    eventArgs.codeKey = e.codeKey;
                    eventArgs.key = String.fromCharCode(e.which);
                    eventArgs.codeKey = this.getCodeKeyByWitch(e.which);

                    this._onClickEvent.execute(eventArgs);
		};
            }
	} else {
            document.onkeypress = null;
	}
    };
    
    /**
     * Add callback function in event handler on cloch event
     *
     * @param {function} callback
     */
    bindOnClick(callback) {
	this._onClickEvent.add(callback);
    }
    
    /**
     * @param {integer} witchCode
     * @returns {Integer}
     */
    getCodeKeyByWitch(witchCode) {
	    switch (witchCode) {
            case 1105:case 1025: case 96:case 126: return 192;
            case 33:case 49: return 49;
            case 34:case 50: return 50;
            case 8470:case 51: return 51;
            case 59:case 52: return 52;
            case 37:case 53: return 53;
            case 58:case 54: return 54;
            case 63:case 55: return 55;
            case 42:case 56: return 56;
            case 40:case 57: return 57;
            case 41:case 48: return 48;
            case 45:case 95: return 173;
            case 43:case 61: return 61;

            case 81: case 113: case 1081: case 1049: return 81;
            case 87: case 119: case 1062: case 1094: return 87;
            case 69: case 101: case 1059: case 1091: return 69;
            case 82: case 114: case 1082: case 1050: return 82;
            case 84: case 116: case 1045: case 1077: return 84;
            case 89: case 121: case 1053: case 1085: return 89;
            case 85: case 117: case 1043: case 1075: return 85;
            case 73: case 105: case 1096: case 1064: return 73;
            case 79: case 111: case 1065: case 1097: return 79;
            case 80: case 112: case 1079: case 1047: return 80;
            case 91: case 123: case 1061: case 1093: return 219;
            case 93: case 125: case 1066: case 1098: return 221;
            case 92: case 124: case 47: return 220;

            case 97: case 65: case 1092: case 1060: return 65;
            case 115: case 83: case 1099: case 1067: return 83;
            case 100: case 68: case 1074: case 1042: return 68;
            case 102: case 70: case 1072: case 1040: return 70;
            case 103: case 71: case 1087: case 1055: return 71;
            case 104: case 72: case 1088: case 1056: return 72;
            case 106: case 74: case 1086: case 1054: return 74;
            case 107: case 75: case 1083: case 1051: return 75;
            case 108: case 76: case 1076: case 1044: return 76;
            case 1078: case 1046: return 59;
            case 39: case 1101: case 1069: return 222;
            case 13: return 13;

            case 122: case 90: case 1103: case 1071: return 90;
            case 120: case 88: case 1095: case 1063: return 88;
            case 99: case 67: case 1089: case 1057: return 67;
            case 118: case 86: case 1084: case 1052: return 86;
            case 98: case 66: case 1080: case 1048: return 66;
            case 110: case 78: case 1090: case 1058: return 78;
            case 109: case 77: case 1100: case 1068: return 77;
            case 60: case 1073: case 1041: return 188;
            case 46: case 62: case 1102: case 1070: return 190;
            case 44: return 191;

            case 32: return 32;
	}
    };
};
