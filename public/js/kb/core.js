import WSClient from '../utils/ws.client.js';
import View from './view.js';
import StringGenerator from '../utils/string.generator.js';
import Keyboard from '../utils/keyboard.js';
import C from '../resources/constants.js';

export default class KeyboardCore {
    get lg () { return this._lg };
    set lg (language) { this._lg = language };
        
    /**
     * @param {Object} connection
     */
    constructor(connection, lg) {
        this._options = {};
        this._counter = 0;
        this._lg = lg;
        this.viewManager = new View();
        this._ws = new WSClient();
        this.enable = false;
        
        connection.messageCallback = (data) => {
            this.receiveProcessor(data);
        };
        
        connection.connectCallback = () => {
            this.viewManager.enable(this.viewManager._connectButton);
        };
        this._ws.connect(connection);
    };

    disconnect() {
        this._ws.disconnect();
    }

    /**
     * Keyboard press event handler
     *
     * @param {Object} eventArgs
     */
    eventClick(eventArgs) {
        if (this.enable) {
            var isValid = (this.viewManager.compare(eventArgs.key) === true);
            this.viewManager.keyFill(eventArgs.codeKey, isValid ? this.viewManager.colorRight : this.viewManager.colorError);

            setTimeout(() => {
                this.viewManager.keyFill(eventArgs.codeKey, this.viewManager.colorDefault);
            }, 100);

            if (!this.viewManager.getStatus()){
                this.viewManager.reset();
            }

            this.processingKey(isValid); 
        }
        
    };

    /**
     * @param {Boolean} isValid
     */
    processingKey(isValid) {
        if (isValid || this.isTraining()) {
            this._counter++;
            
            if (this._counter == this._options.speed) {
                this.fightData(isValid);
                this._counter = 0;
            }
        } else {
            if (!this.isTraining()) {
                this._counter--;
            }
        }
    };

    /**
     * Check the room is training
     * @return {bool}
     */
    isTraining() {
        return this._options[C.Parameter.ROOM_TYPE] === C.Parameter.ROOM_TYPE_TRAINING;
    };

    /**
     * Start training
     * @param {Number} count <p>Count words</p>
     */
    start(count) {
        var string = StringGenerator.generate(count, this._lg);
        
        this.keyboardManager = new Keyboard();
        this.keyboardManager.bindOnClick((eventArgs) => {
            return this.eventClick(eventArgs);
        });
        
        this.keyboardManager.enable = true;
        this.viewManager.start(string);
    };
    
    /**
     * @param {string} login
     */
    connectUser(login) {
        let data = {};
        data[C.Parameter.TYPE] = C.Type.CONNECT_USER;
        data[C.Parameter.LOGIN] = login;
        
        this._ws.send(this.compile(data));
    }
    
    /**
     * @param {Object} options
     */
    connectRoom(options) {
        this._options[C.Parameter.ROOM_TYPE] = options[C.Parameter.ROOM_TYPE];
        options[C.Parameter.TYPE] = C.Type.CONNECT_ROOM;

        this._ws.send(this.compile(options));
    }
    
    fightData(isValid) {
        let data = {};
        let power = (this._options.info && this._options.info.power) ? this._options.info.power : 1;
        data[C.Parameter.TYPE] = C.Type.FIGHT_DATA;
        data[C.Parameter.ID] = this._options.id;
        data[C.Parameter.VALUE] = this.isTraining() ? isValid : power;
        data[C.Parameter.LOGIN] = this._options.login;

        this._ws.send(this.compile(data));
    }
    
    disconnectRoom() {
        let data = {};
        this._options[C.Parameter.ROOM_TYPE] = null;
        data[C.Parameter.TYPE] = C.Type.DISCONNECT_ROOM;
        data[C.Parameter.ID] = this._options.id;
        
        this._ws.send(this.compile(data));
        this.viewManager.reset();
        this.viewManager.removeUserBlocks();
    }
    
    refreshOptions(data) {
        for (let key in data) {
            if (key == C.Parameter.USERS_INFO) {
                let users = data[key];
                for (let number in users) {
                    if (users[number].login == this._options.login) {
                        this._options['info'] = users[number]; 
                    }
                }
            } else {
                this._options[key] = data[key];
            }
        }
    }
    
    receiveProcessor(string) {
        let data = this.parse(string);
        this.refreshOptions(data);
        
        switch(data.type) {
            case C.SystemType.COUNT_CONNECTED_USER:
                this.viewManager.updateCountConnectedUser(data);
                break;
            case C.SystemType.COUNT_ACTIVE_ROOM:
                this.viewManager.updateCountActiveRoom(data);
                break;
            case C.SystemType.SERVER_STATE_INFO:
                this.viewManager.updateServerStateInfo(data);
                break;
            case C.Type.CONNECT_ROOM:
                for (let key in data.users_info) {
                    this.viewManager.createUserBlock(data.users_info[key]);
                }
                break;
            case C.Type.DISCONNECT_ROOM:
                this.viewManager.removeUserBlock(data.login);
                break;
            case C.Type.FIGHT_DATA:
                for (let key in data.users_info) {
                    this.viewManager.updateUserBlock(data.users_info[key]);
                    
                    if (data.users_info[key].login == this._options.login && parseInt(data.users_info[key].current_life) >= 0 ) {
                        this.viewManager.disableWorkItems();
                        this.enable = false;
                    }
                }
                break;
            case C.Type.START_FIGHT:
                this.start(100);
                this.viewManager.enableWorkItems();
                this.enable = true;
                break;
            case C.Type.END_FIGHT:
                for (let key in data.users_info) {
                    this.viewManager.updateUserBlock(data.users_info[key]);
                }
                if (this._ws.isConnected) {
                    this._webSocket.close();
                    this.isConnected = false;
                    
                    this.viewManager.disableWorkItems();
                    this.enable = false;
                }
                break;
        }
    }
    
    /**
     * @param {string} input
     * @returns {Object}
     */
    parse(input) {
        var output = {};
        
        input.split(C.Parameter.DELIMITER_PARAMS).forEach(function(value) {
            var data = value.split(C.Parameter.DELIMITER_KEY_VALUE);
            var value = (data[0] == C.Parameter.USERS_INFO) ? JSON.parse(data[1]) : data[1];
            
            output[data[0]] = value;
        });
        
        return output;
    }
    
    /**
     * 
     * @param {Object} input
     * @returns {string}
     */
    compile(input) {
        let output = [];
        
        for (let key in input) {
            output.push(key + C.Parameter.DELIMITER_KEY_VALUE + input[key]);
        }
        
        return output.join(C.Parameter.DELIMITER_PARAMS);
    }
};