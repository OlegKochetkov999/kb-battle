import $ from 'jquery';
import C from '../resources/constants.js';

const MAX_LINE_SYMBOL = 65;
const COLOR_RIGHT = '#4CBD50';
const COLOR_ERROR = '#BD4C4C';
const COLOR_DEFAULT = 'white';

export default class View {
    get colorRight() {return COLOR_RIGHT;};
    get colorError() {return COLOR_ERROR;};
    get colorDefault() {return COLOR_DEFAULT;};
    
    constructor() {
        this._statusWork = false;
        this._statusWin = false;

        this._trueSpan = $("#input-text span.true");
        this._falseSpan = $("#input-text span.false");
        this._currentSpan = $("#input-text span.current");
        this._remainingSpan = $("#input-text span.remaining");
        this._hidesSpan = $("#input-text span.hides");
        this._countConnectedUser = $('#count-connected-user .status-value');
        this._countActiveRoom = $('#count-active-room .status-value');
        
        this._usersContainer = $('#users');
        this._userTemplate = this._usersContainer.find('#user-template');
        
        this._connectButton = $('#connect');
        this._disconnectButton = $('#disconnect');
        
        this._input = $('#input-text');
        this._keyboard = $('.block-keyboard');
        
        this._hideClass = 'hide';
        this._disabledClass = 'disabled';
        
        this.reset();
    }

    /**
     * Replace symbol in right category
     * @param char
     */
    _applyChar(char){
	this._trueSpan.text(this._trueSpan.text() + char);
	this._falseSpan.text("".trim());
        
   	let rem = this._remainingSpan.text();
	this._currentSpan.text(rem.substring(0, 1));
	this._remainingSpan.text(rem.substring(1, rem.length));
    };

    /**
     * Create new string line
     */
    _remapText() {
	let text = "";
        let count = 0;
	let literals = this._hidesSpan.text().trim().split(' ');

	for (var i = 0; i < literals.length; i++) {
            count += literals[i].length;

            if ((count + 1) < MAX_LINE_SYMBOL){
		text += literals[i] + ' ';
                count++;
            }else{
                count -= literals[i].length;
 		break;
            }
	}

	this._textRemain = text.substring(1, text.length - 1);
	this._textCurrent = text.substring(0, 1);
	let newHide = this._hidesSpan.text().substring(text.length, this._hidesSpan.text().length);

	this._remainingSpan.text(this._textRemain);
	this._currentSpan.text(this._textCurrent);
	this._trueSpan.text('');
	this._falseSpan.text('');
	this._hidesSpan.text(newHide);
    };

    /**
     * Check training is finished
     * @returns {boolean}
     */
    getStatus() {
        if ((this._remainingSpan.text().length === 0) &&
            (this._falseSpan.text().length === 0) &&
            (this._currentSpan.text().length === 0)) {
                if (this._hidesSpan.text().length === 0){
                    return false;
		} else {
                    this._remapText();
		}
        }
            
        return true;
    };

    reset() {
	this._remainingSpan.text('');
	this._currentSpan.text('');
	this._trueSpan.text('');
	this._falseSpan.text('');
	this._hidesSpan.text('');
    };

    /**
     * Create trainig view
     * @param {String} string
     */
    start(string) {
	this.reset();

	this._hidesSpan.text(string.trim());
	this._remapText();

        this._statusWork = true;
	this._statusWin = false;
    };

    /**
     * Draw key
     * @param {Number} code <p>Key code</p>
     * @param {String} color
     */
    keyFill(code, color) {
	$(`#key_${code}`).css({'background-color': color});
    };

    /**
     * @param {String} char
     * @returns {boolean}
     */
    compare(char) {
	let result = false;

	if (this._statusWork === false) {
            return false;
        }

   	if (this._falseSpan.text().length === 0) {
            if (char === this._currentSpan.text()) {
		this._applyChar(char);
		result = true;
   	    } else {
		this._falseSpan.text(this._currentSpan.text());
		this._currentSpan.text("");
            }
   	} else {	
            if (char === this._falseSpan.text()){
		this._applyChar(char);
                result = true;
            }
   	}

	if (this.getStatus() === false) {
            this._statusWork = false;
            this._statusWin = true;
        }

	return result;
    };
    
    /**
     * Create block with user information from data object
     *
     * @param {Object} data
     */
    createUserBlock(data) {
        if ($(`#${data[C.Parameter.LOGIN]}`).length == 0) {
            var $template = this._userTemplate.clone();
            
            $template.find('.login').html(data[C.Parameter.LOGIN]);
            $template.find('.current-life').html(data[C.Parameter.CURRENT_LIFE]);
            $template.attr('id', data[C.Parameter.LOGIN]);
            $template.addClass('user');
            $template.removeClass(this._hideClass);

            this._usersContainer.append($template);
        }
    }
    
    /**
     * Remove block with user information by login
     *
     * @param {String} login
     */
    removeUserBlock(login) {
        $(`#${login}`).remove();
    }
    
    /**
     * Remove all user blocks
     */
    removeUserBlocks() {
        $('#users').find('.user').remove();
    }
    
    /**
     * Update block with user information from data object
     *
     * @param {Object} data
     */
    updateUserBlock(data) {
        let $block = this._usersContainer.find(`#${data[C.Parameter.LOGIN]}`);
        
        if ($block.length > 0) {
            $block.find('.current-life').html(data[C.Parameter.CURRENT_LIFE]);
            
            if (data[C.Parameter.CURRENT_LIFE] == 0) {
                $block.addClass(this._disabledClass);
                
                if (data[C.Parameter.RESULT]) {
                    let result = data[C.Parameter.RESULT];
                    
                    let string = `<p>Count: ${ result[C.Parameter.RESULT_COUNT_CHARS] }</p>
                                  <p>Time: ${ result[C.Parameter.RESULT_TIME] }</p>
                                  <p>Speed: ${ result[C.Parameter.RESULT_SPEED] }</p>
                                  <p>Mistakes: ${ result[C.Parameter.RESULT_MISTAKES] }</p>`;
                    $block.find('.status').html(string);
                } else {
                    $block.find('.status').html('lose');
                }
            }
        }
    }
    
    /**
     * Update information about count of connected users
     *
     * @param {Object} data
     */
    updateCountConnectedUser(data) {
        this._countConnectedUser.find('.current').html(data.value);
        
        if (typeof data.users_info !== undefined) {
            let $options = $('.count-users-block .menu-options');
            $options.find('.option:not(.label)').remove();
            
            $.each(data.users_info, function (key, value) {
                let $option = $options.find('.option').clone();
                $option.removeClass('label');
                $option.find('label').html(value);
                $options.append($option);
            }); 
        }
    }
    
    /**
     * Update information about active room
     *
     * @param {Object} data
     */
    updateCountActiveRoom(data) {
        this._countActiveRoom.find('.current').html(data.value);
        
        if (typeof data.rooms_info !== undefined) {
            let $rooms = $('.rooms-list');
            $rooms.find('.row-room:not(.label)').remove();
            let i = 1;

            $.each(JSON.parse(data.rooms_info), function (key, value) {
                let $room = $rooms.find('.row-room').clone();
                $room.removeClass('label');
                
                $room.find('.room-number').html(`<span data-room="${value.id}">${i++}</span>`);
                $room.find('.room-count-users').html(value.countUsers);
                $room.find('.room-speed').html(value.speed);
                $rooms.append($room);
            }); 
        }
    }
    
    /**
     * Update server state info
     */
    updateServerStateInfo(data) {
        $('#server-run-time').html(data[C.Parameter.SERVER_RUNTIME]);
        $('#server-count-users').html(data[C.Parameter.COUNT_USERS]);
        $('#server-count-rooms').html(data[C.Parameter.COUNT_ROOMS]);
    }
    
    toggleActiveConnectButton() {
        $('.connection-button').toggleClass(this._hideClass);
    }
    
    enableWorkItems() {
        this._input.removeClass(this._disabledClass);
        this._keyboard.removeClass(this._disabledClass);
    }
    
    disableWorkItems() {
        this._input.addClass(this._disabledClass);
        this._keyboard.addClass(this._disabledClass);
    }
    
    clearUsersBlock() {
        this._usersContainer.removeAll('.user');
    }
    
    enable(element) {
        element.removeClass(this._disabledClass);
    }
    
    disable(element) {
        element.addClass(this._disabledClass);
    }
};
