<?php

namespace App\Server\Entity\Environment;

class Parameter 
{
    const TYPE = "type";
    const LOGIN = "login";
    const ID = "id";
    const VALUE = "value";
    const LIFE = "life";
    const TEXT = "text";
    const CURRENT_LIFE = "current_life";
    
    const ROOM_TYPE = 'room_type';
    const ROOM_SPEED = 'speed';
    const ROOM_STATE = 'room_state';
    const ROOMS_INFO = 'rooms_info';
    
    const RESULT = 'result';
    const RESULT_COUNT_CHARS = 'result_count_chars';
    const RESULT_TIME = 'result_time';
    const RESULT_SPEED = 'result_speed';
    const RESULT_MISTAKES = 'result_mistakes';
    
    const SERVER_RUNTIME = 'server_runtime';
    const COUNT_ROOMS = 'countRooms';
    const COUNT_USERS = "countUsers";
    const COUNT_CHARS = "countChars";
    
    const USER_INFO = "user_info";
    const USERS_INFO = "users_info";
    
    const STATUS = "status";
    const STATUS_OK = "ok";
    const STATUS_ERR = "err";
    
    const USER_POWER = "power";
    const USER_WIN = "win";
    const USER_DIE = "die";
    
    const DELIMITER_PARAMS = '|';
    const DELIMITER_KEY_VALUE = '=';
    
    const ROOM_TYPE_SIMPLE = 'room_type_simple';
    const ROOM_TYPE_TRAINING = 'room_type_training';
}
