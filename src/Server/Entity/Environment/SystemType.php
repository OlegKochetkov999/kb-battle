<?php

namespace App\Server\Entity\Environment;

class SystemType 
{
    const COUNT_CONNECTED_USER = 'ccu';
    const COUNT_ACTIVE_ROOM = 'car';
    const SERVER_STATE_INFO = 'ssi';
}
