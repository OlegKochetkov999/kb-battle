<?php

namespace App\Server\Entity\Environment;

class Type 
{
    const CONNECT_USER = "cu";
    const CONNECT_ROOM = "cr";
    const DISCONNECT_ROOM = "dr";
    const START_FIGHT = "sf";
    const END_FIGHT = "ef";
    const FIGHT_DATA = "fd";
    const INFO = "in";
}
