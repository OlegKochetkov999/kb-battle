<?php
namespace App\Server\Entity;

use App\Server\Entity\Environment\Parameter;

class User
{
    /**
     * @var int
     */
    private $id;
    
    /**
     * @var string 
     */
    private $icon;
    
    /**
     * @param int
     */
    private $life = 20;
    
    /**
     * @param int
     */
    private $currentLife = 20;
    
    /**
     * @var int 
     */
    private $power = 1;
    
    /**
     * @var int 
     */
    private $heal = 1;
    
    /**
     *
     * @var bool 
     */
    private $ready = false;
    
    /**
     *
     * @var bool 
     */
    private $lose = false;
    
    /**
     *
     * @var bool 
     */
    private $blocked;

    /**
     *
     * @var string
     */
    private $login;
    
    /**
     * @param string $login
     * @param int $id
     */
    public function __construct(string $login = null, int $id = null) 
    {
        $this->id = $id;
        $this->login = $login;
    }
    
    /**
     * @param int $damage
     * @return int
     */
    public function setDamage(int $damage = 0): int
    {
        $this->currentLife = $this->currentLife - $damage;
        
        if ($this->currentLife <= 0) {
            $this->currentLife = 0;
            $this->lose = true;
        }
        
        return $this->currentLife;
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return int
     */
    public function getLife(): int
    {
        return $this->life;
    }
    
    /**
     * @param int $life
     */
    public function setLife(int $life)
    {
        $this->life = $life;
    }
    
    /**
     * @return int
     */
    public function getCurrentLife(): int
    {
        return $this->currentLife;
    }
    
    /**
     * @param int $life
     */
    public function setCurrentLife(int $life)
    {
        if ($this->life > $this->currentLife) {
            $this->currentLife = $life;
        }
    }
    
    /**
     * @return int
     */
    public function getPower() : int
    {
        return $this->power;
    }
    
    /**
     * @return string
     */
    public function getIcon() : string
    {
        return $this->icon;
    }
    
    /**
     * @return string
     */
    public function getLogin() : string
    {
        return $this->login;
    }
    
    /**
     * @return bool
     */
    public function isReady() : bool
    {
        return $this->ready;
    }
    
    /**
     * @param bool $ready
     */
    public function setReady(bool $ready = false)
    {
        $this->ready = $ready;
    }
    
    /**
     * @void
     */
    public function healing()
    {
        $this->currentLife += $this->heal;
        
        if ($this->currentLife > $this->life) {
            $this->currentLife = $this->life;
        }
    }
    
    /**
     * @return bool
     */
    public function isLose() : bool
    {
        return $this->lose;
    }
    
    /**
     * @param bool $blocked
     */
    public function setBlocked(bool $blocked = false)
    {
       $this->blocked = $blocked;
    }
    
    /**
     * @return bool
     */
    public function isBlocked(): bool
    {
       return $this->blocked;
    }
    
    /**
     * @return array
     */
    public function getInfo(): array
    {
        return [
            Parameter::LOGIN => $this->login,
            Parameter::LIFE => $this->life,
            Parameter::CURRENT_LIFE => $this->currentLife,
            Parameter::USER_POWER => $this->power,
            'isLose' => $this->isLose()
        ];
    }
}
