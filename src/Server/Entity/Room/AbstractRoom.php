<?php

namespace App\Server\Entity\Room;

use App\Server\Entity\User;
use App\Server\Entity\Room\RoomParameters;
use App\Server\Entity\Environment\Parameter;

abstract class AbstractRoom
{
    /**
     * @var string
     */
    protected $id;
    
    /**
     * @var bool
     */
    protected $launched;
    
    /**
     * @var string
     */
    protected $type;
    
    /**
     * @var int
     */
    protected $speed;

    /**
     * @var \DateTime
     */
    protected $startTime;
    
    /**
     * @var \DateTime
     */
    protected $endTime;
    
    /**
     * @param User $user
     * @param RoomParameters $params
     */
    public function __construct(User $user, RoomParameters $params)
    {
        $this->startTime = new \DateTime();
        $this->speed = $params->speed ?? 5;
        
        $this->launched = false;
        $this->id = md5((new \DateTime())->format('Y-m-d H:i:s'));
        $this->addUser($user);
    }
    
    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
    
    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * @return bool
     */
    public function isLaunched(): bool
    {
        return $this->launched;
    }
    
    /**
     * @param bool $launched
     */
    public function setLaunched(bool $launched)
    {
        $this->launched = $launched;
    }
    
    /**
     * @return array
     */
    abstract public function getUsers(): array;
    
    /**
     * @return array
     */
    abstract public function getUsersInfo();
    
    /**
     * @param User|null $user
     */
    abstract public function removeUser(User $user = null);
    
    /**
     * @param User|null $user
     */
    abstract public function addUser(User $user = null);
    
    /**
     * @return bool
     */
    abstract public function isComplete(): bool;
    
    /**
     * @param RoomParameters $params
     * @return bool
     */
    public function isAvailable(RoomParameters $params)
    {
        return false;
    }
    
    /**
     * @return array
     */
    public function getInfo(): array
    {
        return [
            Parameter::ID => $this->id,
            Parameter::COUNT_USERS => $this->countUsers,
            Parameter::ROOM_STATE => $this->launched,
            Parameter::ROOM_SPEED => $this->speed
        ];
    }
    
    /**
     * @return bool
     */
    abstract public function isFinished(): bool;

    /**
     * @param User $user
     * @param mixed $value
     */
    abstract public function process(User $user, $value);
    
    public function __toString(): string
    {
        return "room #" . $this->id;
    }
}
