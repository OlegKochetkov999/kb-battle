<?php

namespace App\Server\Entity\Room;

use App\Server\Entity\Environment\Parameter;
use App\Server\Entity\User;
use App\Server\Entity\Room\RoomParameters;
use App\Helper\DateIntervalHelper;

class TrainingRoom extends AbstractRoom
{
    /**
     * @var User
     */
    protected $user;
    
    /**
     * @var int
     */
    protected $countChars;
    
    /**
     * @var int
     */
    protected $countLeftChars;
    
    /**
     * @var 0
     */
    private $mistakes = 0;
    
    /**
     * @param User $user
     * @param RoomParameters $params
     */
    public function __construct(User $user, RoomParameters $params)
    {
        parent::__construct($user, $params);
        
        $this->countChars = $this->countLeftChars = $params->countChars ?? 50;
        $this->type = Parameter::ROOM_TYPE_TRAINING;
    }
    
    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
    
    /**
     * @void
     */
    public function reduceCountChars()
    {
        $this->countLeftChars -= $this->speed;
    }
    
    /**
     * @void
     */
    public function incrementMistakes()
    {
        $this->mistakes += 1;
    }
    
    /**
     * @return array|User[]
     */
    public function getUsers(): array
    {
        return [$this->user];
    }
    
    /**
     * @return array
     */
    public function getUsersInfo(): array
    {
        $info = $this->user->getInfo();
        $info[Parameter::CURRENT_LIFE] = $this->countLeftChars;
        $info[Parameter::LIFE] = $this->countChars;
        
        if ($this->endTime) {
            $diff = $this->startTime->diff($this->endTime);

            $info[Parameter::RESULT] = [
                Parameter::RESULT_COUNT_CHARS => $this->countChars,
                Parameter::RESULT_TIME => $diff->format(DateIntervalHelper::formatting($diff)),
                Parameter::RESULT_SPEED => round(floatval($this->countChars / DateIntervalHelper::inMinutes($diff)), 2),
                Parameter::RESULT_MISTAKES => $this->mistakes
            ];
        }
        
        return [$info];
    }
    
    /**
     * @param User|null
     */
    public function removeUser(User $user = null)
    {
        $this->users = null;
    }
    
    /**
     * @param User|null
     */
    public function addUser(User $user = null)
    {
        $this->user = $user;
    }
    
    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return !is_null($this->user);
    }
    
    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        if ($this->countLeftChars <= 0) {
            $this->endTime = new \DateTime();
            return true;
        }
        
        return false;
    }
    
    /**
     * @param User $user
     * @param mixed $value
     */
    public function process(User $user, $value)
    {
        if ($value === 'true') {
            $this->reduceCountChars(); 
        } else {
            $this->incrementMistakes();
        }
    }
}
