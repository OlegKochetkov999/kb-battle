<?php

namespace App\Server\Entity\Room;

class RoomParameters
{
    /**
     * @var int
     */
    public $speed;

    /**
     * @var int
     */
    public $countUsers;
    
    /**
     * @var int
     */
    public $countChars;
    
    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        foreach ($params as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = intval($value);
            }
        }
    }
}
