<?php

namespace App\Server\Entity\Room;

use App\Server\Entity\Environment\Parameter;
use App\Server\Entity\User;
use App\Server\Entity\Room\RoomParameters;

class Room extends AbstractRoom
{
    /**
     * @var array|User[]
     */
    protected $users = [];
    
    /**
     * @var int
     */
    protected $countUsers;
    
    /**
     * @param User $user
     * @param RoomParameters $params
     */
    public function __construct(User $user, RoomParameters $params)
    {
        parent::__construct($user, $params);
        
        $this->countUsers = $params->countUsers;
        $this->type = Parameter::ROOM_TYPE_SIMPLE;
    }
    
    /**
     * @return int
     */
    public function getCountUsers(): int
    {
        return $this->countUsers;
    }
    
    /**
     * @return array|User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }
    
    /**
     * @return array|User[]
     */
    public function getUsersInfo(): array
    {
        $data = [];
        
        foreach ($this->users as $user) {
            $data[] = $user->getInfo();
        }
        
        return $data;
    }
    
    /**
     * @param User|null $user
     */
    public function removeUser(User $user = null)
    {
        if ($user) {
            unset($this->users[$user->getId()]); 
        }
    }
    
    /**
     * @param User|null $user
     */
    public function addUser(User $user = null)
    {
        if (!isset($this->users[$user->getId()]) && !$this->isComplete()) {
            $this->users[$user->getId()] = $user;
        }
    }
    
    /**
     * @return bool
     */
    public function isComplete(): bool
    {
        return (count($this->users) === $this->countUsers);
    }
    
    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        $loses = array_filter ($this->users, function ($user) {
            return $user->isLose();
        });

        if (count($loses) == (count($this->users) - 1)) {
            $this->endTime = new \DateTime();
            return true;
        }
        
        return false;
    }
    
    /**
     * @param RoomParameters $params
     * @return bool
     */
    public function isAvailable(RoomParameters $params): bool
    {
        return ($this->countUsers === $params->countUsers && !$this->launched && $this->speed = $params->speed);
    }
    
    /**
     * @return array
     */
    public function getInfo(): array
    {
        $info = parent::getInfo();
        $info[Parameter::COUNT_USERS] = $this->countUsers;
        
        return $info;
    }
    
    /**
     * @param User $user
     * @param mixed $value
     */
    public function process(User $user, $value)
    {
        $user->healing();
        
        foreach ($this->users as $currentUser) {
            if ($user->getId() != $currentUser->getId()) {
                $currentUser->setDamage((int)$value);
            }
        }
    }
}
