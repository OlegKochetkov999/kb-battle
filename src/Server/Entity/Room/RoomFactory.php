<?php

namespace App\Server\Entity\Room;

use App\Server\Entity\Environment\Parameter;
use App\Server\Entity\Room\RoomParameters;
use App\Server\Entity\Room\AbstractRoom;
use App\Server\Entity\Room\Room;
use App\Server\Entity\Room\TrainingRoom;
use App\Server\Entity\User;

class RoomFactory
{
    /**
     * @param string $type
     * @param User $user
     * @param RoomParameters $params
     * @return AbstractRoom
     */
    public static function create(string $type, User $user, RoomParameters $params)
    {
        switch ($type) {
            case Parameter::ROOM_TYPE_SIMPLE:
                return new Room($user, $params);
            case Parameter::ROOM_TYPE_TRAINING:
                return new TrainingRoom($user, $params);
        }
    }
}
