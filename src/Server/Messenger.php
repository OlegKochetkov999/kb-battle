<?php

namespace App\Server;

use App\Server\Entity\Environment\SystemType;
use App\Server\Entity\Environment\Type;
use App\Server\Entity\Environment\Parameter;
use App\Server\Entity\Room\AbstractRoom;
use App\Server\Entity\User;
use App\Server\Helper\Message;
use App\Server\ServerState;
use Ratchet\ConnectionInterface;

class Messenger
{
    /**
     * @var ServerState
     */
    protected $state;

    /**
     * @param ServerState $state
     */
    public function __construct(ServerState $state)
    {
        $this->state = $state;
    }

    /**
     * @param string $message
     * @param ConnectionInterface|null $conn
     */
    private function send(string $message, ConnectionInterface$conn = null)
    {
        if ($conn) {
            $conn->send($message);
        } else {
            $this->sendAllClients($message);
        }
    }
    
    /**
     * @param ConnectionInterface|null $conn
     */
    public function sendConnectionUserState(ConnectionInterface $conn = null)
    {
        $this->send(Message::create(Type::CONNECT_USER, [Parameter::STATUS => Parameter::STATUS_OK]), $conn);
    }
    
    /**
     * @param ConnectionInterface|null $conn
     */
    public function sendActiveUsersInfo(ConnectionInterface $conn = null)
    {
        $users = $this->state->getUsersInfo();
        
        $message = Message::create(SystemType::COUNT_CONNECTED_USER, [
                Parameter::VALUE => count($users),
                Parameter::USERS_INFO => json_encode($users)
            ]);

        $this->send($message, $conn);
    }
    
    /**
     * @param ConnectionInterface|null $conn
     */
    public function sendActiveRoomsInfo(ConnectionInterface $conn = null)
    {
        $roomInfo = $this->state->getInfoActiveRoom();
        
        $message = Message::create(SystemType::COUNT_ACTIVE_ROOM, [
                Parameter::VALUE => count($roomInfo),
                Parameter::ROOMS_INFO => json_encode($roomInfo)
            ]);
        
        $this->send($message, $conn);
    }
    
    /**
     * @param ConnectionInterface|null $conn
     */
    public function sendServerStateInfo(ConnectionInterface $conn = null)
    {
        $message = Message::create(SystemType::SERVER_STATE_INFO, $this->state->getStatistics());
        
        $conn->send($message, $conn);
    }

    /**
     * @param ConnectionInterface|null $conn
     * @param string $error
     */
    public function sendError(ConnectionInterface $conn = null, string $error = '')
    {
        $message = Message::create(Type::INFO, [Parameter::STATUS => Parameter::STATUS_ERR, Parameter::VALUE => $error]);
        
        $conn->send($message, $conn);
    }
    
    /**
     * @param AbstractRoom $room
     */
    public function sendStartTraining($room)
    {
        $this->sendRoomClients($room, Message::create(Type::START_FIGHT));
    }
    
    /**
     * @param AbstractRoom $room
     */
    public function sendRoomState($room)
    {
        $this->sendRoomClients(
            $room, 
            Message::create(Type::CONNECT_ROOM, [
                    Parameter::ID => $room->getId(), 
                    Parameter::STATUS => Parameter::STATUS_OK,
                    Parameter::ROOM_SPEED => $room->getSpeed(),
                    Parameter::USERS_INFO => json_encode($room->getUsersInfo())
                ]
            )
        );
    }

    /**
     * @param AbstractRoom $room
     * @param User|null $user
     */
    public function sendDisconnectRoom($room, User $user = null)
    {
        if ($user) {
            $message = Message::create(Type::DISCONNECT_ROOM, [Parameter::LOGIN => $user->getLogin()]);
            $this->sendRoomClients($room, $message);
        }
    }

    /**
     * @param AbstractRoom $room
     * @param string $type
     */
    public function sendRoomUsersInfo($room, string $type = Type::FIGHT_DATA)
    {
        $this->sendRoomClients(
            $room, 
            Message::create($type, [Parameter::USERS_INFO => json_encode($room->getUsersInfo())])
        );
    }
    
    /**
     * Send message to connection room users
     * @param AbstractRoom $room
     * @param string $message
     */
    private function sendRoomClients($room, string $message)
    {
        foreach ($room->getUsers() as $user) {
            if ($client = $this->state->getClientByUser($user->getId())) {
                $client->send($message);
            }
        }
    }
    
    /**
     * Send message to all users
     * @param string $message
     */
    private function sendAllClients(string $message)
    {
        foreach ($this->state->getClients() as $client) {
            $client->send($message);
        }
    }
}
