<?php

namespace App\Server;

use Ratchet\ConnectionInterface;
use App\Server\Entity\Room\Room;
use App\Server\Entity\User;
use App\Server\Entity\Room\RoomParameters;
use App\Server\Entity\Environment\Parameter;
use App\Helper\DateIntervalHelper;

class ServerState
{
    /**
     * @var \DateTime
     */
    protected $startDate;
    
    /**
     * @var \SplObjectStorage 
     */
    protected $clients;
    
    /**
     * Structure array [id_room => Room]
     * @var array 
     */
    protected $rooms; 
    
    /**
     * Structure array [id_client => User]
     * @var array 
     */
    protected $relation;
    
    /**
     * @var array
     */
    protected $statistics = [
        Parameter::COUNT_USERS => 0,
        Parameter::COUNT_ROOMS => 0
    ];

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->rooms = [];
        $this->relation = [];
        $this->startDate = new \DateTime();
    }
    
    /**
     * @return string
     */
    public function getRuntime(): string
    {
        $now = new \DateTime();
        $diff = $now->diff($this->startDate);
        
        return $diff->format(DateIntervalHelper::formatting($diff));
    }
    
    /**
     * @return \SplObjectStorage
     */
    public function getClients(): \SplObjectStorage
    {
        return $this->clients;
    }
    
    /**
     * Connection with client
     *
     * @param ConnectionInterface $conn
     */
    public function attachClient(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
    }
    
    /**
     * Disconnection with client
     *
     * @param ConnectionInterface $conn
     */
    public function detachClient(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        $this->detachUser($conn->resourceId);
    }
    
    /**
     * Get all users info
     *
     * @return array
     */
    public function getUsersInfo(): array
    {
        $users = [];
        foreach ($this->relation as $user) {
            $users[] = $user->getLogin();
        }
        
        return $users;
    }
    
    /**
     * Attach user in array
     * 
     * @param User $user
     */
    public function attachUser(User $user)
    {
        $this->relation[$user->getId()] = $user;
        $this->statistics[Parameter::COUNT_USERS] = $this->statistics[Parameter::COUNT_USERS] + 1;
    }
    
    /**
     * Detach user from array
     * 
     * @param int $id
     */
    public function detachUser(int $id)
    {
        unset($this->relation[$id]);
    }
    
    /**
     * @param int $id
     * @return User
     */
    public function getUser(int $id)
    {
        return $this->relation[$id] ?? null;
    }
    
    /**
     * Attach Room in array
     * 
     * @param AbstractRoom $room
     */
    public function attachRoom($room)
    {
        $this->rooms[$room->getId()] = $room;
        $this->statistics[Parameter::COUNT_ROOMS] = $this->statistics[Parameter::COUNT_ROOMS] + 1;
    }
    
    /**
     * Detach Room from array
     * 
     * @param string $id
     */
    public function detachRoom(string $id)
    {
        unset($this->rooms[$id]);
    }
    
    /**
     * Get Room from array by id
     * 
     * @param string $id
     * @return int
     */
    public function getRoom(string $id = null)
    {
        return $this->rooms[$id] ?? null;
    }
    
    /**
     * Get first available room
     * 
     * @param RoomParameters $params
     * @return Room|null
     */
    public function getAvailableRoom(RoomParameters $params)
    {
        foreach ($this->rooms as $room) {
            if ($room->isAvailable($params)) {
                return $room;
            }
        }
        
        return null;
    }
    
    /**
     * get Id connection by user
     * @param int $id
     * @return ConnectionInterface
     */
    public function getClientByUser(int $id)
    {
        foreach ($this->clients as $client) {
            if ($client->resourceId === $id) {
                return $client;
            }
        }
    }
    
    /**
     * Return count of active rooms
     *
     * @return int
     */
    public function getCountActiveRoom()
    {
        $rooms = array_filter($this->rooms, function ($room) {
            return !$room->isLaunched();
        });
        
        return count($rooms);
    }
    
    /**
     * Return info about active rooms
     *
     * @return array
     */
    public function getInfoActiveRoom()
    {
        $info = [];
        
        foreach ($this->rooms as $room) {
            if (!$room->isLaunched() && $room->getType() !== Parameter::ROOM_TYPE_TRAINING) {
                $info[] = $room->getInfo();
            }
        }
        
        return $info;
    }
    
    /**
     * Return room by client id
     *
     * @param int $id
     * @return Room|null
     */
    public function getClientRoom(int $id)
    {
        foreach ($this->rooms as $room) {
            foreach ($room->getUsers() as $user) {
                if ($user->getId() === $id) {
                    return $room;
                }
            }
        }
    }
    
    /**
     * @return array
     */
    public function getStatistics(): array
    {
        $data = $this->statistics;
        $data[Parameter::SERVER_RUNTIME] = $this->getRuntime();
        
        return $data;
    }
}
