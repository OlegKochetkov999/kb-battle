<?php

namespace App\Server\Helper;

use App\Server\Entity\Environment\Parameter;

class Message 
{
    /**
     * @param string $type
     * @param array $array
     * @return string
     */
    public static function create(string $type, array $array = []): string
    {
        $format = Parameter::TYPE . Parameter::DELIMITER_KEY_VALUE . $type;

        foreach ($array as $key => $value) {
            if (!is_null($value)) {
               $format .= Parameter::DELIMITER_PARAMS . $key . Parameter::DELIMITER_KEY_VALUE . $value; 
            }
        }
        
        return $format;
    }

    /**
     * Get parameters array from input string
     * @param string $message
     * @return array
     */
    public static function parse(string $message): array
    {
        $array = [];
        
        foreach(explode(Parameter::DELIMITER_PARAMS, $message) as $option){
            $params = explode(Parameter::DELIMITER_KEY_VALUE, $option);
            if (count($params) > 1) {
                $array[$params[0]] = $params[1];
            }
        }
        
        return $array;
    }
}
