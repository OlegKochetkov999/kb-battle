<?php

namespace App\Server;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use App\Server\Entity\Environment\Type;
use App\Server\Entity\Environment\Parameter;
use App\Server\Entity\Room\RoomFactory;
use App\Server\Entity\User;
use App\Server\Entity\Room\RoomParameters;
use App\Server\Helper\Message;
use App\Server\ServerState;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class Application implements MessageComponentInterface 
{
    const STYLE_ERROR = 'e';
    const STYLE_IMPORTANT = 'i';
    
    const ENV_PROD = 'prod';
    const ENV_DEV = 'dev';
    
    /**
     * @var OutputInterface
     */
    protected $output;
    
    /**
     * @var ServerState
     */
    protected $state;

    /**
     * @var Messenger
     */
    protected $messenger;
    
    /**
     * @param OutputInterface $output
     * @param ServerState $state
     * @param string $env
     */
    public function __construct(OutputInterface $output, ServerState $state, $env = self::ENV_PROD)
    {
        $this->state = $state;
        $this->output = $output;
        $this->messenger = new Messenger($this->state);
        
        if ($env === self::ENV_DEV) {
            $this->output->getFormatter()->setStyle(self::STYLE_ERROR, new OutputFormatterStyle('white', 'red'));
            $this->output->getFormatter()->setStyle(self::STYLE_IMPORTANT, new OutputFormatterStyle('white', 'green'));
        }
        
        $this->output->writeLn("Start ws server");
    }

    /**
     * Connection with client
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->state->attachClient($conn);
        
        $this->messenger->sendActiveUsersInfo($conn);
        $this->messenger->sendActiveRoomsInfo($conn);
        $this->messenger->sendServerStateInfo($conn);
        
        $this->output->writeLn("New connection (<i>{$conn->resourceId}</>)");
    }

    /**
     * Processing incoming request
     * @param ConnectionInterface $conn
     * @param string $msg  
     */
    public function onMessage(ConnectionInterface $conn, $msg) 
    {
        $this->output->writeLn("msg: <i>$msg</>");
        $input = Message::parse($msg);
        
        try {
            switch ($input[Parameter::TYPE]) {
                case Type::CONNECT_USER:
                    $this->connectUser($input, $conn);
                    break;
                case Type::CONNECT_ROOM:
                    $this->connectRoom($input, $conn);
                    break;
                case Type::DISCONNECT_ROOM:
                    $this->disconnectRoom($input[Parameter::ID] ?? null, $conn);
                    break;
                case Type::FIGHT_DATA:
                    $this->fightData($input, $conn);
                    break;
            }
        } catch (\Exception $e) {
            $this->output->writeLn(sprintf("msg: <e>[%s] %s (%s %s)</>", $e->getCode(), $e->getMessage(), $e->getFile(), $e->getLine()));
            $this->messenger->sendError($conn, 'Something went wrong');
        }
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->processCloseConnection($conn, "User <i>{$conn->resourceId}</> disconnected");
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception          $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->processCloseConnection($conn, "Error: <e>{$e->getMessage()}</>");
    }
    
    /**
     * Create new user and add this in array clients
     *
     * @param array $input
     * @param ConnectionInterface $conn
     */
    private function connectUser(array $input, ConnectionInterface $conn)
    {
        $login = !empty($input[Parameter::LOGIN]) ? $input[Parameter::LOGIN] : $conn->resourceId;
        $this->state->attachUser(new User($login, $conn->resourceId));
        
        $this->messenger->sendConnectionUserState($conn);
        $this->messenger->sendActiveUsersInfo();
    }
    
    /**
     * Connection User on existing room or creating new room
     *
     * @param array $input
     * @param ConnectionInterface $conn
     */
    private function connectRoom(array $input, ConnectionInterface $conn)
    {
        $user = $this->state->getUser($conn->resourceId);
        $type = $input[Parameter::ROOM_TYPE] ?? Parameter::ROOM_TYPE_SIMPLE;
        $params = new RoomParameters($input);
        $room = isset($input[Parameter::ID]) ? $this->state->getRoom($input[Parameter::ID]) : 
                                               $this->state->getAvailableRoom($params);

        if ($room) {
            $room->addUser($user);
        } else {
            $room = RoomFactory::create($type, $user, $params);
        }
        
        $this->state->attachRoom($room);
        $this->messenger->sendRoomState($room);
        $this->messenger->sendActiveRoomsInfo();
        
        if ($room->isComplete()) {
            $room->setLaunched(true);
            $this->messenger->sendStartTraining($room);
        }
    }
    
    /**
     * Disconnect user from room and remove room
     *
     * @param mixed $room
     * @param ConnectionInterface $conn
     */
    private function disconnectRoom($room = null, ConnectionInterface $conn)
    {
        if ($room === null) {
            $room = $this->state->getClientRoom($conn->resourceId);
        } else if (is_string($room)) {
            $room = $this->state->getRoom($room);
        }

        if ($room) {
            $user = $this->state->getUser($conn->resourceId);
            $room->removeUser($user);
            $this->messenger->sendDisconnectRoom($room, $user);
        }
    }
    
    /**
     * @param array $input
     * @param ConnectionInterface $conn
     */
    private function fightData(array $input, ConnectionInterface $conn)
    {
        $room = $this->state->getRoom($input[Parameter::ID] ?? null);
        $user = $this->state->getUser($conn->resourceId);
        
        if ($user->isLose() || !$room || $room->isFinished()) {
            return;
        }

        $room->process($user, $input[Parameter::VALUE] ?? 0);

        $this->messenger->sendRoomUsersInfo($room);

        if ($room->isFinished()) {
            $this->processRoomFinish($room);
        }
    }
    
    /**
     * @param AbstractRoom $room
     */
    private function processRoomFinish($room)
    {
        $this->state->detachRoom($room->getId());

        $this->messenger->sendRoomUsersInfo($room, Type::END_FIGHT);
        $this->messenger->sendActiveRoomsInfo();
    }
    
    /**
     * @param ConnectionInterface $conn
     * @param string $message
     */
    private function processCloseConnection(ConnectionInterface $conn, string $message)
    {
        $this->state->detachClient($conn);
        $this->output->writeLn($message);
        $this->disconnectRoom(null, $conn);

        $this->messenger->sendActiveUsersInfo();
        $conn->close();
    }
}
