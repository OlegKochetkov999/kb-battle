<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Helper\WsServerHelper;

/**
 * @Route("/")
 */
class IndexController extends Controller
{
    /**
     * @Route("/")
     * @param Request $request
     */
    public function indexAction(Request $request)
    {
        return $this->render('index.html.twig', [
            "isServerRun" => WsServerHelper::isServerRun()
        ]);
    }
    
    /**
     * @Route("/change-language", options={"expose"=true}, name="app_change-language")
     * @param Request $request
     */
    public function changeLanguageAction(Request $request)
    {
        return $this->json($this->renderView('keyboard.html.twig', ["currentLg" => $request->get('lg', 'En')]));
    }
}
