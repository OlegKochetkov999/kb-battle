<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Server\Entity\Environment\Parameter;
use App\Server\Entity\Environment\Type;
use App\Server\Entity\Environment\SystemType;
use ReflectionClass;

class ConstantDumpCommand extends ContainerAwareCommand
{
    private $classes = [Parameter::class, Type::class, SystemType::class];
    
    protected function configure()
    {
        $this
            ->setName('kb:constant:dump')
            ->setDescription('Dumping php constant to json')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeLn($this->getDescription());
        $result = [];
        foreach ($this->classes as $class) {
            $reflection = new ReflectionClass($class);
            $result[$reflection->getShortName()] = $reflection->getConstants();
            $output->writeLn($reflection->getName());
        }
        
        $path = $this->getContainer()->get('kernel')->getRootDir() . '/../public/js/resources/constants.js';
        
        file_put_contents($path, 'export default ' . json_encode($result));
        $output->writeLn("Dumping finished. File created in $path");
    }
}
