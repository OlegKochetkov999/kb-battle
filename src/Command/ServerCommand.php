<?php

namespace App\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\Server\Application;
use App\Server\ServerState;

class ServerCommand extends ContainerAwareCommand
{
    const ACTION_START = 'start';
    const ACTION_STOP = 'stop';
    
    private $server;
    
    protected function configure()
    {
        $this
            ->setName('kb:server')
            ->addArgument('action', InputArgument::OPTIONAL, 'Action with server', self::ACTION_START)
            ->addArgument('port', InputArgument::OPTIONAL, 'Port of server', getenv('APP_WS_SERVER_PORT'))
            ->setDescription('Start ws server command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument('action');
        
        if ($action === self::ACTION_START && !$this->server) {
            $this->server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        new Application(new ConsoleOutput(), new ServerState(), getenv('APP_ENV'))
                    )
                ),
                $input->getArgument('port')
            );
            
            $this->server->run();
        }
    }
}
