<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use OK\WebSocket\Terminal\Terminal;
use OK\WebSocket\Client\Client;

class TestCommand extends ContainerAwareCommand
{
    const TEST_INTERACTIVE = 'i';

    protected function configure()
    {
        $this
            ->setName('kb:client')
            ->addArgument('test', InputArgument::OPTIONAL, 'Test type', self::TEST_INTERACTIVE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $test = $input->getArgument('test');
        
        switch ($test) {
            case 'i':
                $client = new Terminal(new Client(getenv('APP_WS_SERVER_URL'), getenv('APP_WS_SERVER_PORT')));
                $client->run();
                break;
            default:
                $this->runTest($test);
                break;
        }
    }
    
    private function runTest(string $test)
    {
        $classTest = "App\\Client\\Tests\\$test";
        
        if (class_exists($classTest)) {
            $test = new $classTest();
            $test->start();
        }
    }
}
