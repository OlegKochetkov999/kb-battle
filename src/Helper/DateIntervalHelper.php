<?php

namespace App\Helper;

class DateIntervalHelper
{
    /**
     * Summing two \DateInterval's
     *
     * @param \DateInterval $firstInterval
     * @param \DateInterval $secondInterval
     * @param bool $isConversion
     *
     * @return \DateInterval
     */
    public static function sum(\DateInterval $firstInterval = null, \DateInterval $secondInterval = null, bool $isConversion = false)
    {
        if (!$firstInterval || !($firstInterval instanceof \DateInterval)) {
            return $secondInterval;
        }
        
        if (!$secondInterval || !($secondInterval instanceof \DateInterval)) {
            return $firstInterval;
        }
        
        $firstInterval->y += $secondInterval->y;
        $firstInterval->m += $secondInterval->m;
        $firstInterval->d += $secondInterval->d;
        $firstInterval->h += $secondInterval->h;
        $firstInterval->i += $secondInterval->i;
        $firstInterval->s += $secondInterval->s;
        
        return $isConversion ? self::conversion($firstInterval) : $firstInterval;
    }
    
    /**
     * Recalculation hours, minutes, seconds in the interval
     *
     * @param \DateInterval $interval
     * @return \DateInterval
     */
    public static function conversion(\DateInterval $interval)
    {
        if ($interval->s > 60) {
            $interval->i += intval($interval->s / 60);
            $interval->s = $interval->s % 60;
        }
        
        if ($interval->i > 60) {
            $interval->h += intval($interval->i / 60);
            $interval->i = $interval->i % 60;
        }
        
        if ($interval->h > 24) {
            $interval->d += intval($interval->h / 24);
            $interval->h = $interval->h % 24;
        }
        
        return $interval;
    }
    
    /**
     * @param \DateInterval $interval
     * @return string
     */
    public static function formatting(\DateInterval $interval)
    {
        $format = '%I:%S';
            
        if ($interval->h > 0) {
            $format = '%H:' . $format;
        }
        
        if ($interval->d > 0) {
            $format = '%d дн.  ' . $format;
        }
        
        if ($interval->m > 0) {
            $format = '%m м. ' . $format;
        }

        if ($interval->y > 0) {
            $format = '%y г. ' . $format;
        }
        
        return $format;
    }
    
    /**
     * @param \DateInterval $dateInterval
     * @return int seconds
     */
    public static function inSeconds(\DateInterval $dateInterval)
    {
        $reference = new \DateTimeImmutable();
        $endTime = $reference->add($dateInterval);

        return $endTime->getTimestamp() - $reference->getTimestamp();
    }
    
    /**
     * @param \DateInterval $dateInterval
     * @return float
     */
    public static function inMinutes(\DateInterval $dateInterval)
    {
        $seconds = self::inSeconds($dateInterval);
        $minutes = intval($seconds / 60);
        
        $part = (($seconds % 60) * 100) / 60;
        
        return $minutes + ($part / 100);
    }
}
