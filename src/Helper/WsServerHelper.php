<?php
namespace App\Helper;

use Symfony\Component\Process\Process;

class WsServerHelper
{
    const SERVER_IDENTITY = "kb:server";
    
    /**
     * @return bool
     */
    public static function isServerRun()
    {
        $process = new Process("ps aux | grep " . self::SERVER_IDENTITY);
        $process->run();

        return substr_count($process->getOutput(), self::SERVER_IDENTITY) > 2;
    }
}
