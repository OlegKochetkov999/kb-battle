<?php

namespace App\Client\Tests;

use OK\WebSocket\Client\Client;

class TrainingMode implements TestSocketInterface
{
    /**
     * @var Client
     */
    private $client;
    
    public function __construct()
    {
        $this->client = new Client(getenv('APP_WS_SERVER_URL'), getenv('APP_WS_SERVER_PORT'));
    }
    
    public function start(): ?string
    {
        var_dump('-------');
        $this->client->send('type=cu|login=oleg');
        $data = $this->client->receive();
        var_dump($data);
        var_dump('-------');
        $this->client->send('type=cr');
        $data = $this->client->receive();
        var_dump($data);
        var_dump('-------');
        $this->client->close();
        
        return $this->getResult();
    }
    
    public function getResult(): string
    {
        return 'Test finished';
    }
}
