<?php

namespace App\Client\Tests;

interface TestSocketInterface
{
    /**
     * @return string|null
     */
    public function start(): ?string;
    
    /**
     * @return string
     */
    public function getResult(): string;
}
