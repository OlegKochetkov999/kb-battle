COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

## Clear cache
cache-clear:
	@printf "${COLOR_COMMENT}Clearing cache:${COLOR_RESET}\n"
	@test -f bin/console && bin/console cache:clear --no-warmup || rm -rf var/cache/*

## Clear server cache
clean-server:
	rm -rf var/cache/*
	rm -rf var/logs/*

# Remove all generated js and css files
clean-client:
	rm -rf node_modules
	rm -rf public/dist
	rm -rf public/resources/constants.js

# Run unit tests
phpunit:
	php vendor/bin/phpunit

## Composer install
composer:
	composer install

# Download and generate js
client-resources:
	bin/console kb:constant:dump
	bin/console fos:js-routing:dump --format=json --target=public/dist/js/fos_js_routing.json
	npm install
	npm run build

# Compile local less to css
css:
	lessc public/less/index.less public/dist/css/index.css
	lessc public/less/site.less public/dist/css/site.css
	lessc public/less/variables.less public/dist/css/variables.css

# Create Symlink for vendor css
css-vendor:
	rm -f public/dist/css/materialize.min.css
	ln node_modules/materialize-css/dist/css/materialize.min.css public/dist/css/materialize.min.css

# Create cron jobs
cron-restart-server-command:
	./utils/cron-restart-server-command.sh

## Build frontend	
frontend: client-resources css css-vendor

## Rebuild frontend	
frontend@rebuild: clean-client frontend

## Rebuild after a git pull on master
rebuild: clean-server composer frontend
